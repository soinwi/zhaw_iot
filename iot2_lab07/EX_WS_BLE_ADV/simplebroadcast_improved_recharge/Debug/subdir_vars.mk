################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../cc26x0f128.cmd 

C_SRCS += \
../ccfg.c \
../main.c \
../radio.c \
../rtc.c \
../system.c 

C_DEPS += \
./ccfg.d \
./main.d \
./radio.d \
./rtc.d \
./system.d 

OBJS += \
./ccfg.obj \
./main.obj \
./radio.obj \
./rtc.obj \
./system.obj 

OBJS__QUOTED += \
"ccfg.obj" \
"main.obj" \
"radio.obj" \
"rtc.obj" \
"system.obj" 

C_DEPS__QUOTED += \
"ccfg.d" \
"main.d" \
"radio.d" \
"rtc.d" \
"system.d" 

C_SRCS__QUOTED += \
"../ccfg.c" \
"../main.c" \
"../radio.c" \
"../rtc.c" \
"../system.c" 


