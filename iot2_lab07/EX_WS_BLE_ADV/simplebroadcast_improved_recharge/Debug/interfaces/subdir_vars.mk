################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../interfaces/board-i2c.c \
../interfaces/board-spi.c 

C_DEPS += \
./interfaces/board-i2c.d \
./interfaces/board-spi.d 

OBJS += \
./interfaces/board-i2c.obj \
./interfaces/board-spi.obj 

OBJS__QUOTED += \
"interfaces\board-i2c.obj" \
"interfaces\board-spi.obj" 

C_DEPS__QUOTED += \
"interfaces\board-i2c.d" \
"interfaces\board-spi.d" 

C_SRCS__QUOTED += \
"../interfaces/board-i2c.c" \
"../interfaces/board-spi.c" 


