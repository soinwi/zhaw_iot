package ch.ines.zhaw.zhaw_blens;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import ch.ines.zhaw.bleines.R;

/**
 * Created by dalyan on 04.04.16.
 */
public class Settings extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_layout);

        SharedPreferences sp = getSharedPreferences("filterPrefs", Activity.MODE_PRIVATE);
        int vendorFilter = sp.getInt("vendorFilter", -1);
        int IDFilter = sp.getInt("IDFilter", -1);
        final EditText vendorEdit = (EditText) findViewById(R.id.editTextVendor);
        final EditText IDEdit = (EditText) findViewById(R.id.editTextID);

        if(vendorFilter >= 0)vendorEdit.setText(""+vendorFilter);
        else vendorEdit.setText("");

        if (IDFilter >= 0)IDEdit.setText(""+IDFilter);
        else IDEdit.setText("");

        final Button button = (Button) findViewById(R.id.buttonSave);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                int vendorFilter;
                int IDFilter;
            //    Toast.makeText(getApplicationContext(), "Button was pressed!:-)\n", Toast.LENGTH_SHORT).show();//debug
                /*save prefs*/
                SharedPreferences sp = getSharedPreferences("filterPrefs", Activity.MODE_PRIVATE);
                SharedPreferences.Editor editor = sp.edit();
                if(vendorEdit.getText().toString()==""){
                    editor.putInt("vendorFilter", -1);
                }else{
                    try {
                        vendorFilter = Integer.parseInt(vendorEdit.getText().toString());
                    } catch(NumberFormatException nfe) {
                        System.out.println("Could not parse " + nfe);
                        vendorFilter = -1;
                    }
                    editor.putInt("vendorFilter", vendorFilter);
                }
                if(IDEdit.getText().toString()==""){
                    editor.putInt("IDFilter", -1);
                }else{
                    try {
                        IDFilter = Integer.parseInt(IDEdit.getText().toString());
                    } catch(NumberFormatException nfe) {
                        System.out.println("Could not parse " + nfe);
                        IDFilter = -1;
                    }
                    editor.putInt("IDFilter", IDFilter);
                }
                editor.commit();
                goToMain();
            }
        });
    }

    private void goToMain(){
        Intent intent = new Intent(this, Sensors.class);
        startActivity(intent);
    }
}