
#include "board.h"
#include "debug.h"
#include "uart.h"
#include "fifo.h"

#define UART_FIFO_SIZE	100

#define UART_BAUDRATE		256000


Uart_t Uart1;

Fifo_t fifoUart;
uint8_t uartBuffer[16];

void DebugInit( void )
{	
		//Uart init
		FifoInit( &fifoUart, uartBuffer, UART_FIFO_SIZE );
		Uart1.FifoTx = fifoUart;
		UartInit(&Uart1, 0, UART_TX,UART_RX );
		UartConfig( &Uart1, TX_ONLY, UART_BAUDRATE, UART_8_BIT, UART_1_STOP_BIT, NO_PARITY, NO_FLOW_CTRL );
}

void DebugPrintHeader( void )
{
		printf(" ******************************************************************************* \r\n");
		printf(" *  _____       ______   ____ \r\n");
		printf(" * |_   _|     |  ____|/ ____|  Institute of Embedded Systems \r\n");
		printf(" *   | |  _ __ | |__  | (___    Wireless Group \n");
		printf(" *   | | | '_ ||  __|  |___ |   Zuercher Hochschule Winterthur \r\n");
		printf(" *  _| |_| | | | |____ ____) |  (University of Applied Sciences) \r\n");
		printf(" * |_____|_| |_|______|_____/   8401 Winterthur, Switzerland \r\n");
		printf(" * \r\n");
		printf(" ******************************************************************************* \r\n");
}

uint8_t Debug_PutChar( uint8_t character )
{
		return UartPutChar( &Uart1, character );
}

uint8_t Debug_GetChar( void )
{
		uint8_t data = 0x00;
		UartGetChar( &Uart1, &data );
		return data;
}
