
#ifndef __DEBUG_H__
#define __DEBUG_H__

#include "board.h"
#include <stdio.h>

void DebugInit( void );
void DebugPrintHeader( void );
uint8_t Debug_PutChar( uint8_t character );
uint8_t Debug_GetChar( void );


#endif
