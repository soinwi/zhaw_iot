/*
#include "board.h"
#include "spi-board.h"
#include "stm32l1xx_spi.h"
#include "stm32l1xx_gpio.h"
*/
#include "sensor-board.h"

//Spi_t SensorsSpi;

//uint16_t SpiInOut( Spi_t *obj, uint16_t outData )
//{
//    if( ( obj == NULL ) || ( obj->Spi ) == NULL )
//    {
//        while( 1 );
//    }
//    
//    while( SPI_I2S_GetFlagStatus( obj->Spi, SPI_I2S_FLAG_TXE ) == RESET );
//    SPI_I2S_SendData( obj->Spi, outData );
//    while( SPI_I2S_GetFlagStatus( obj->Spi, SPI_I2S_FLAG_RXNE ) == RESET );
//    return SPI_I2S_ReceiveData( obj->Spi );
//}

/*
uint16_t adx1362_deviceId;
uint16_t bme280_deviceId;
Gpio_t gpio_bme280;
Gpio_t gpio_adxl362;

void SensorsWrite( Gpio_t *obj, uint8_t addr, uint8_t data );
uint8_t SensorsRead( Gpio_t *obj, uint8_t addr );
void SensorsWriteBuffer( Gpio_t *obj, uint8_t addr, uint8_t *buffer, uint8_t size);
void SensorsReadBuffer( Gpio_t *obj, uint8_t addr, uint8_t *buffer, uint8_t size );
*/
/*
void BoardInitSensors(void)
{

		// Init SPI for Sensor extension board
		Spi2Init( &SensorsSpi, SENSORS_MOSI, SENSORS_MISO, SENSORS_SCLK, NC );

		// Init SPI-CS (nss) pins for ADX1362 and BME280
		GpioInit( &gpio_adxl362, SENSORS_ADXL362_NSS, PIN_OUTPUT, PIN_PUSH_PULL, PIN_PULL_UP, 1 );
		GpioInit( &gpio_bme280, SENSORS_BME280_NSS, PIN_OUTPUT, PIN_PUSH_PULL, PIN_PULL_UP, 1 );

		// Toggle SPI-CS pin of BME280 once to active SPI mode on slave.
		GpioWrite( &gpio_bme280, 0 );
		uint8_t i;		
		for (i=0; i<100; i++)
				;;
		GpioWrite( &gpio_bme280, 1 );

		///////////////// ADX1362 //////////////////

		//NSS = 0;
		GpioWrite( &gpio_adxl362, 0 );

		// Read mode
		SpiInOut( &SensorsSpi, 0x0B );

		// Register Device ID
		SpiInOut( &SensorsSpi, 0x02 );

		// Get result
		adx1362_deviceId = SpiInOut( &SensorsSpi, 0 );

		//NSS = 1;
		GpioWrite( &gpio_adxl362, 1 );	



		///////////// BME280 /////////////////////

		//NSS = 0;
		GpioWrite( &gpio_bme280, 0 );

		// Register Device ID
		SpiInOut( &SensorsSpi, 0xD0 );

		// Get result
		bme280_deviceId = SpiInOut( &SensorsSpi, 0x00 );

		//NSS = 1;
		GpioWrite( &gpio_bme280, 1 );
}

uint8_t performAccelerometerMeasurement( uint16_t resultdata [])
{
	uint16_t xaxis = 0;
	uint16_t yaxis = 0;
	uint16_t zaxis = 0;
	
	// do measurements...
	
	
	resultdata[0] = xaxis;
	resultdata[1] = yaxis;
	resultdata[2] = zaxis;
	
	return 0;
}

uint8_t performBME280Measurement( uint32_t resultdata [])
{
	int32_t humidity = 0;
	int32_t pressure = 0;
	int32_t temperature = 0;
	
	// do measurements...
	uint8_t mode = BME280_OSAMPLE_16;
	GpioWrite( &gpio_bme280, 0 );
	SpiInOut( &SensorsSpi, BME280_REGISTER_CONTROL_HUM );
	SpiInOut( &SensorsSpi,  mode);
	GpioWrite( &gpio_bme280, 1 );	
	
	uint8_t meas = mode << 5 | mode << 2 | 1;
	GpioWrite( &gpio_bme280, 0 );
	SpiInOut( &SensorsSpi, BME280_REGISTER_CONTROL );
	SpiInOut( &SensorsSpi, meas );
	GpioWrite( &gpio_bme280, 1 );	
	
	uint32_t timer;
	for (timer = 0; timer < 0x7FFFFF; timer++)
		;;
	
	GpioWrite( &gpio_bme280, 0 );
	SpiInOut( &SensorsSpi, BME280_REGISTER_TEMP_DATA );
	uint8_t msb = SpiInOut( &SensorsSpi, 0 );
	SpiInOut( &SensorsSpi, BME280_REGISTER_TEMP_DATA + 1 );
	uint8_t lsb = SpiInOut( &SensorsSpi, 0 );
	SpiInOut( &SensorsSpi, BME280_REGISTER_TEMP_DATA + 2 );
	uint8_t xlsb = SpiInOut( &SensorsSpi, 0 );
	GpioWrite( &gpio_bme280, 1 );	
	temperature = ((msb << 16) | (lsb << 8) | xlsb) >> 4;
	
	resultdata[0] = humidity;
	resultdata[1] = pressure;
	resultdata[2] = temperature;
	
	return 0;
}


void SensorsWrite( Gpio_t *obj, uint8_t addr, uint8_t data )
{
    SensorsWriteBuffer( obj, addr, &data, 1 );
}

uint8_t SensorsRead( Gpio_t *obj, uint8_t addr )
{
    uint8_t data;
    SensorsReadBuffer( obj, addr, &data, 1 );
    return data;
}

void SensorsWriteBuffer( Gpio_t *obj, uint8_t addr, uint8_t *buffer, uint8_t size)
{
    uint8_t i;

    //NSS = 0;
    GpioWrite( obj, 0 );

    SpiInOut( &SensorsSpi, addr );
    for( i = 0; i < size; i++ )
    {
        SpiInOut( &SensorsSpi, buffer[i] );
    }

    //NSS = 1;
    GpioWrite( obj, 1 );
}

void SensorsReadBuffer( Gpio_t *obj, uint8_t addr, uint8_t *buffer, uint8_t size )
{
    uint8_t i;

    //NSS = 0;
    GpioWrite( obj, 0 );

    SpiInOut( &SensorsSpi, addr );

    for( i = 0; i < size; i++ )
    {
        buffer[i] = SpiInOut( &SensorsSpi, 0 );
    }

    //NSS = 1;
    GpioWrite( obj, 1 );
}
*/
