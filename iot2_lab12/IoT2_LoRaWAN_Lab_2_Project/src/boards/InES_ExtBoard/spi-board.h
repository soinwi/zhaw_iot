/*
 / _____)             _              | |
( (____  _____ ____ _| |_ _____  ____| |__
 \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 _____) ) ____| | | || |_| ____( (___| | | |
(______/|_____)_|_|_| \__)_____)\____)_| |_|
    (C)2013 Semtech

Description: Bleeper board SPI driver implementation

License: Revised BSD License, see LICENSE.TXT file include in the project

Maintainer: Miguel Luis and Gregory Cristian
*/
#ifndef __SPI_MCU_H__
#define __SPI_MCU_H__

//#include "stm32l1xx.h"
/*
#include "board.h"
#include "spi.h"
#include "spi-board.h"
#include "stm32l1xx_spi.h"
#include "stm32l1xx_gpio.h"
*/

/*!
 * SPI driver structure definition
 */
struct Spi_s
{
    SPI_TypeDef *Spi;
    Gpio_t Mosi;
    Gpio_t Miso;
    Gpio_t Sclk;
    Gpio_t Nss;
};

/*
void SensorsWriteBuffer( Spi_t *spiobj, Gpio_t *gpioobj, uint8_t addr, uint8_t *buffer, uint8_t size);
void SensorsReadBuffer( Spi_t *spiobj, Gpio_t *gpioobj, uint8_t addr, uint8_t *buffer, uint8_t size );
*/
#endif  // __SPI_MCU_H__
