/******************************************************************************
bme280.h
BME280 Arduino and Teensy Driver
Marshall Taylor @ SparkFun Electronics
May 20, 2015
https://github.com/sparkfun/BME280_Breakout

This code is released under the [MIT License](http://opensource.org/licenses/MIT).
Please review the LICENSE.md file included.
******************************************************************************
Modified by: ZHAW Institute of Embedded Systems (gugm), 02.05.2016
- Removed I2C capabilities
- Added ST hardware driver components
******************************************************************************/

#include "bme280.h"
#include "stdint.h"
#include <math.h>

#include "sensor-board.h"

#include "stm32l1xx.h"
#include "stm32l1xx_spi.h"
#include "stm32l1xx_gpio.h"
#include "board.h"
#include "spi-board.h"

//****************************************************************************//
//
//  Settings and configuration
//
//****************************************************************************//

//Class SensorSettings.  This object is used to hold settings data.  The application
//uses this classes' data directly.  The settings are adopted and sent to the sensor
//at special times, such as .begin.  Some are used for doing math.
//
//This is a kind of bloated way to do this.  The trade-off is that the user doesn't
//need to deal with #defines or enums with bizarre names.
//
//A power user would strip out SensorSettings entirely, and send specific read and
//write command directly to the IC. (ST #defines below)
//
struct SensorSettings
{
	
  //Main Interface and mode settings
	uint8_t commInterface;
	uint8_t I2CAddress;
	PinNames chipSelectPin;
	
	Spi_t *spidriver;				//SPI object type
	Gpio_t *chipSelectGPIO;
	
	uint8_t runMode;
	uint8_t tStandby;
	uint8_t filter;
	uint8_t tempOverSample;
	uint8_t pressOverSample;
	uint8_t humidOverSample;

} settings;

//Used to hold the calibration constants.  These are used
//by the driver as measurements are being taking
struct SensorCalibration
{
	uint16_t dig_T1;
	int16_t dig_T2;
	int16_t dig_T3;
	
	uint16_t dig_P1;
	int16_t dig_P2;
	int16_t dig_P3;
	int16_t dig_P4;
	int16_t dig_P5;
	int16_t dig_P6;
	int16_t dig_P7;
	int16_t dig_P8;
	int16_t dig_P9;
	
	uint8_t dig_H1;
	int16_t dig_H2;
	uint8_t dig_H3;
	int16_t dig_H4;
	int16_t dig_H5;
	uint8_t dig_H6;
	
} calibration;

//settings
int32_t t_fine;

Spi_t SensorsSpi;
Gpio_t gpio_bme280;
Gpio_t gpio_adxl362;


//Constructor -- Specifies default configuration
void BME280_init( void )
{
	settings.commInterface = SPI_MODE;
	settings.I2CAddress = 0x77; //Ignored for SPI_MODE
	settings.chipSelectPin = SENSORS_BME280_NSS;
	settings.spidriver = &SensorsSpi;
	settings.chipSelectGPIO = &gpio_bme280;
	settings.runMode = 3; //  3, Normal mode
	//settings.runMode = 0; // 0, Sleep mode
	settings.tStandby = 0; //  0, 0.5ms
	settings.filter = 0; //  0, filter off
	//tempOverSample can be:
	//  0, skipped
	//  1 through 5, oversampling *1, *2, *4, *8, *16 respectively
	settings.tempOverSample = 1;
	//pressOverSample can be:
	//  0, skipped
	//  1 through 5, oversampling *1, *2, *4, *8, *16 respectively
    settings.pressOverSample = 1;
	//humidOverSample can be:
	//  0, skipped
	//  1 through 5, oversampling *1, *2, *4, *8, *16 respectively
	settings.humidOverSample = 1;
}


//****************************************************************************//
//
//  Configuration section
//
//  This uses the stored SensorSettings to start the IMU
//  Use statements such as "mySensor.settings.commInterface = SPI_MODE;" to 
//  configure before calling .begin();
//
//****************************************************************************//
uint8_t BME280_begin()
{
	//Check the settings structure values to determine how to setup the device
	uint8_t dataToWrite = 0;  //Temporary variable

	switch (settings.commInterface)
	{
		
	case SPI_MODE:
		// Init SPI for Sensor extension board
		Spi2Init( settings.spidriver, SENSORS_MOSI, SENSORS_MISO, SENSORS_SCLK, NC );

		// Init SPI-CS (nss) pin for BME280
		GpioInit( settings.chipSelectGPIO, settings.chipSelectPin, PIN_OUTPUT, PIN_PUSH_PULL, PIN_PULL_UP, 1 );
		
		GpioInit( &gpio_adxl362, SENSORS_ADXL362_NSS, PIN_OUTPUT, PIN_PUSH_PULL, PIN_PULL_UP, 1 );

	
			// Toggle SPI-CS pin of BME280 once to active SPI mode on slave.
		GpioWrite( &gpio_bme280, 0 );
		GpioWrite( &gpio_bme280, 1 );
		break;

	default:
		break;
	}

	//Reading all compensation data, range 0x88:A1, 0xE1:E7
	
	calibration.dig_T1 = ((uint16_t)((BME280_readRegister(BME280_DIG_T1_MSB_REG) << 8) + BME280_readRegister(BME280_DIG_T1_LSB_REG)));
	calibration.dig_T2 = ((int16_t)((BME280_readRegister(BME280_DIG_T2_MSB_REG) << 8) + BME280_readRegister(BME280_DIG_T2_LSB_REG)));
	calibration.dig_T3 = ((int16_t)((BME280_readRegister(BME280_DIG_T3_MSB_REG) << 8) + BME280_readRegister(BME280_DIG_T3_LSB_REG)));

	calibration.dig_P1 = ((uint16_t)((BME280_readRegister(BME280_DIG_P1_MSB_REG) << 8) + BME280_readRegister(BME280_DIG_P1_LSB_REG)));
	calibration.dig_P2 = ((int16_t)((BME280_readRegister(BME280_DIG_P2_MSB_REG) << 8) + BME280_readRegister(BME280_DIG_P2_LSB_REG)));
	calibration.dig_P3 = ((int16_t)((BME280_readRegister(BME280_DIG_P3_MSB_REG) << 8) + BME280_readRegister(BME280_DIG_P3_LSB_REG)));
	calibration.dig_P4 = ((int16_t)((BME280_readRegister(BME280_DIG_P4_MSB_REG) << 8) + BME280_readRegister(BME280_DIG_P4_LSB_REG)));
	calibration.dig_P5 = ((int16_t)((BME280_readRegister(BME280_DIG_P5_MSB_REG) << 8) + BME280_readRegister(BME280_DIG_P5_LSB_REG)));
	calibration.dig_P6 = ((int16_t)((BME280_readRegister(BME280_DIG_P6_MSB_REG) << 8) + BME280_readRegister(BME280_DIG_P6_LSB_REG)));
	calibration.dig_P7 = ((int16_t)((BME280_readRegister(BME280_DIG_P7_MSB_REG) << 8) + BME280_readRegister(BME280_DIG_P7_LSB_REG)));
	calibration.dig_P8 = ((int16_t)((BME280_readRegister(BME280_DIG_P8_MSB_REG) << 8) + BME280_readRegister(BME280_DIG_P8_LSB_REG)));
	calibration.dig_P9 = ((int16_t)((BME280_readRegister(BME280_DIG_P9_MSB_REG) << 8) + BME280_readRegister(BME280_DIG_P9_LSB_REG)));

	calibration.dig_H1 = ((uint8_t)(BME280_readRegister(BME280_DIG_H1_REG)));
	calibration.dig_H2 = ((int16_t)((BME280_readRegister(BME280_DIG_H2_MSB_REG) << 8) + BME280_readRegister(BME280_DIG_H2_LSB_REG)));
	calibration.dig_H3 = ((uint8_t)(BME280_readRegister(BME280_DIG_H3_REG)));
	calibration.dig_H4 = ((int16_t)((BME280_readRegister(BME280_DIG_H4_MSB_REG) << 4) + (BME280_readRegister(BME280_DIG_H4_LSB_REG) & 0x0F)));
	calibration.dig_H5 = ((int16_t)((BME280_readRegister(BME280_DIG_H5_MSB_REG) << 4) + ((BME280_readRegister(BME280_DIG_H4_LSB_REG) >> 4) & 0x0F)));
	calibration.dig_H6 = ((uint8_t)BME280_readRegister(BME280_DIG_H6_REG));

	//Set the oversampling control words.
	//config will only be writeable in sleep mode, so first insure that.
	BME280_writeRegister(BME280_CTRL_MEAS_REG, 0x00);
	
	//Set the config word
	dataToWrite = (settings.tStandby << 0x5) & 0xE0;
	dataToWrite |= (settings.filter << 0x02) & 0x1C;
	BME280_writeRegister(BME280_CONFIG_REG, dataToWrite);
	
	//Set ctrl_hum first, then ctrl_meas to activate ctrl_hum
	dataToWrite = settings.humidOverSample & 0x07; //all other bits can be ignored
	BME280_writeRegister(BME280_CTRL_HUMIDITY_REG, dataToWrite);
	
	//set ctrl_meas
	//First, set temp oversampling
	dataToWrite = (settings.tempOverSample << 0x5) & 0xE0;
	//Next, pressure oversampling
	dataToWrite |= (settings.pressOverSample << 0x02) & 0x1C;
	//Last, set mode
	dataToWrite |= (settings.runMode) & 0x03;
	//Load the byte
	BME280_writeRegister(BME280_CTRL_MEAS_REG, dataToWrite);
	
	return BME280_readRegister(0xD0);
}

//Strictly resets.  Run .begin() afterwards
void BME280_reset( void )
{
	BME280_writeRegister(BME280_RST_REG, 0xB6);
	
}

//****************************************************************************//
//
//  Pressure Section
//
//****************************************************************************//
float BME280_readFloatPressure( void )
{

	// Returns pressure in Pa as unsigned 32 bit integer in Q24.8 format (24 integer bits and 8 fractional bits).
	// Output value of “24674867” represents 24674867/256 = 96386.2 Pa = 963.862 hPa
	int32_t adc_P = ((uint32_t)BME280_readRegister(BME280_PRESSURE_MSB_REG) << 12) | ((uint32_t)BME280_readRegister(BME280_PRESSURE_LSB_REG) << 4) | ((BME280_readRegister(BME280_PRESSURE_XLSB_REG) >> 4) & 0x0F);
	
	int64_t var1, var2, p_acc;
	var1 = ((int64_t)t_fine) - 128000;
	var2 = var1 * var1 * (int64_t)calibration.dig_P6;
	var2 = var2 + ((var1 * (int64_t)calibration.dig_P5)<<17);
	var2 = var2 + (((int64_t)calibration.dig_P4)<<35);
	var1 = ((var1 * var1 * (int64_t)calibration.dig_P3)>>8) + ((var1 * (int64_t)calibration.dig_P2)<<12);
	var1 = (((((int64_t)1)<<47)+var1))*((int64_t)calibration.dig_P1)>>33;
	if (var1 == 0)
	{
		return 0; // avoid exception caused by division by zero
	}
	p_acc = 1048576 - adc_P;
	p_acc = (((p_acc<<31) - var2)*3125)/var1;
	var1 = (((int64_t)calibration.dig_P9) * (p_acc>>13) * (p_acc>>13)) >> 25;
	var2 = (((int64_t)calibration.dig_P8) * p_acc) >> 19;
	p_acc = ((p_acc + var1 + var2) >> 8) + (((int64_t)calibration.dig_P7)<<4);
	
	p_acc = p_acc >> 8; // /256
	return (float)p_acc;
	
}

float BME280_readFloatAltitudeMeters( void )
{
	float heightOutput = 0;
	
	heightOutput = ((float)-45846.2)*(pow(((float)BME280_readFloatPressure()/(float)101325), 0.190263) - (float)1);
	return heightOutput;
	
}

float BME280_readFloatAltitudeFeet( void )
{
	float heightOutput = 0;
	
	heightOutput = BME280_readFloatAltitudeMeters() * 3.28084;
	return heightOutput;
	
}

//****************************************************************************//
//
//  Humidity Section
//
//****************************************************************************//
float BME280_readFloatHumidity( void )
{
	
	// Returns humidity in %RH as unsigned 32 bit integer in Q22. 10 format (22 integer and 10 fractional bits).
	// Output value of “47445” represents 47445/1024 = 46. 333 %RH
	int32_t adc_H = ((uint32_t)BME280_readRegister(BME280_HUMIDITY_MSB_REG) << 8) | ((uint32_t)BME280_readRegister(BME280_HUMIDITY_LSB_REG));
	
	int32_t var1;
	var1 = (t_fine - ((int32_t)76800));
	var1 = (((((adc_H << 14) - (((int32_t)calibration.dig_H4) << 20) - (((int32_t)calibration.dig_H5) * var1)) +
	((int32_t)16384)) >> 15) * (((((((var1 * ((int32_t)calibration.dig_H6)) >> 10) * (((var1 * ((int32_t)calibration.dig_H3)) >> 11) + ((int32_t)32768))) >> 10) + ((int32_t)2097152)) *
	((int32_t)calibration.dig_H2) + 8192) >> 14));
	var1 = (var1 - (((((var1 >> 15) * (var1 >> 15)) >> 7) * ((int32_t)calibration.dig_H1)) >> 4));
	var1 = (var1 < 0 ? 0 : var1);
	var1 = (var1 > 419430400 ? 419430400 : var1);

	return (float)((var1>>12) >> 10);

}



//****************************************************************************//
//
//  Temperature Section
//
//****************************************************************************//

float BME280_readTempC( void )
{
	// Returns temperature in DegC, resolution is 0.01 DegC. Output value of “5123” equals 51.23 DegC.
	// t_fine carries fine temperature as global value

	//get the reading (adc_T);
	int32_t adc_T = ((uint32_t)BME280_readRegister(BME280_TEMPERATURE_MSB_REG) << 12) | ((uint32_t)BME280_readRegister(BME280_TEMPERATURE_LSB_REG) << 4) | ((BME280_readRegister(BME280_TEMPERATURE_XLSB_REG) >> 4) & 0x0F);

	//By datasheet, calibrate
	int64_t var1, var2;

	var1 = ((((adc_T>>3) - ((int32_t)calibration.dig_T1<<1))) * ((int32_t)calibration.dig_T2)) >> 11;
	var2 = (((((adc_T>>4) - ((int32_t)calibration.dig_T1)) * ((adc_T>>4) - ((int32_t)calibration.dig_T1))) >> 12) *
	((int32_t)calibration.dig_T3)) >> 14;
	t_fine = var1 + var2;
	float output = (t_fine * 5 + 128) >> 8;

	output = output / 100;
	
	return output;
}

float BME280_readTempF( void )
{
	float output = BME280_readTempC();
	output = (output * 9) / 5 + 32;

	return output;
}

//****************************************************************************//
//
//  Utility
//
//****************************************************************************//
void BME280_readRegisterRegion(uint8_t *outputPointer , uint8_t offset, uint8_t length)
{
	//define pointer that will point to the external space
	uint8_t i = 0;
	char c = 0;

	switch (settings.commInterface)
	{

	case SPI_MODE:
		// take the chip select low to select the device:
		GpioWrite( settings.chipSelectGPIO, LOW );
		// send the device the register you want to read:
		SpiInOut( settings.spidriver, offset | 0x80 ); //Ored with "read request" bit
		while ( i < length ) // slave may send less than requested
		{
			c = SpiInOut(settings.spidriver, 0x00); // receive a byte as character
			*outputPointer = c;
			outputPointer++;
			i++;
		}
		// take the chip select high to de-select:
		GpioWrite( settings.chipSelectGPIO, HIGH );
		break;

	default:
		break;
	}

}

uint8_t BME280_readRegister(uint8_t offset)
{
	//Return value
	uint8_t result;
	switch (settings.commInterface) {

	case SPI_MODE:
		// take the chip select low to select the device:
		GpioWrite( settings.chipSelectGPIO, LOW );
		// send the device the register you want to read:
		SpiInOut( settings.spidriver, offset | 0x80 ); //Ored with "read request" bit
		// send a value of 0 to read the first byte returned:
		result = SpiInOut(settings.spidriver, 0x00);
		// take the chip select high to de-select:
		GpioWrite( settings.chipSelectGPIO, HIGH );
		break;

	default:
		break;
	}
	return result;
}

int16_t BME280_readRegisterInt16( uint8_t offset )
{
	uint8_t myBuffer[2];
	BME280_readRegisterRegion(myBuffer, offset, 2);  //Does memory transfer
	int16_t output = (int16_t)myBuffer[0] |(int16_t)(myBuffer[1] << 8);
	
	return output;
}

void BME280_writeRegister(uint8_t offset, uint8_t dataToWrite)
{
	switch (settings.commInterface)
	{

	case SPI_MODE:
		// take the chip select low to select the device:
		GpioWrite( settings.chipSelectGPIO, LOW );
		// send the device the register you want to read:
		SpiInOut( settings.spidriver, offset & 0x7F );
		// send a value of 0 to read the first byte returned:
		SpiInOut(settings.spidriver, dataToWrite);
		// decrement the number of bytes left to read:
		// take the chip select high to de-select:
		GpioWrite( settings.chipSelectGPIO, HIGH );
		break;

	default:
		break;
	}
}
