/*
 * sensor_demo.h
 *
 *  Created on: 24.11.2015
 *      Author: brts
 */

#define SENSOR_READING_PERIOD (CLOCK_SECOND * 20)
#define SENSOR_READING_RANDOM (CLOCK_SECOND << 4)

static struct ctimer bmp_timer, opt_timer, hdc_timer, tmp_timer, mpu_timer;

/*
 * Initialize the BMP280 Pressure Sensor
 *
 * After successfull initialisation, the notify_ready function will be called,
 * which gives notification to the main process to start measurement.
 */
void init_bmp_reading(void);

/*
 * Initialize the OPT3001 Light Sensor
 *
 * After successfull initialisation, the notify_ready function will be called,
 * which gives notification to the main process to start measurement.
 */
void init_opt_reading(void);

/*
 * Initialize the HDV1000 Humidity Sensor
 *
 * After successfull initialisation, the notify_ready function will be called,
 * which gives notification to the main process to start measurement.
 */
void init_hdc_reading(void);

/*
 * Initialize the TMP007 Infrared Thermopile Sensor
 *
 * After successfull initialisation, the notify_ready function will be called,
 * which gives notification to the main process to start measurement.
 */
void init_tmp_reading(void);

/*
 * Initialize the MPU-9250 Gyroscope and Accelerometer Sensor
 *
 * After successfull initialisation, the notify_ready function will be called,
 * which gives notification to the main process to start measurement.
 */
void init_mpu_reading(void);

/*
 * Initialize the Battery Monitor and the Reed Relay (Always On)
 */
void init_sensors_aon(void);

/*
 * activate all the Sensors
 */
void activate_all_sensors(void);

/*
 * Function for formated output of the MPU readings
 */
void print_mpu_reading(int reading);

/*
 * Get and print reading of the battery monitor
 */
void get_batmon_reading(void);

/*
 * Get and print reading of the BMP280 Pressure Sensor
 */
void get_bmp_reading();

/*
 * Get and print reading of the TMP007 Infrared Thermopile Sensor
 */
void get_tmp_reading();

/*
 * Get and print reading of the HDV1000 Humidity Sensor
 */
void get_hdc_reading();

/*
 * Get and print reading of the OPT3001 Light Sensor
 */
void get_light_reading();

/*
 * Get and print reading of the MPU-9250 Gyroscope and Accelerometer Sensor
 */
void get_mpu_reading();




