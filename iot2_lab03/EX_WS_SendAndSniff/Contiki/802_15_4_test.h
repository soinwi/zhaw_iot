/*
 * 802_15_4_test.h
 *
 *  Created on: 11.02.2016
 *      Author: dalyan
 */

#ifndef 802_15_4_TEST_H_
#define 802_15_4_TEST_H_

void rxCallBack(char * packetStart, uint8_t len);

#endif /* 802_15_4_TEST_H_ */
