/*
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

#include "contiki.h"
#include "contiki-lib.h"
#include "contiki-net.h"
#include "net/ip/resolv.h"

#include <string.h>
#include <stdbool.h>

#include "sensors.h"
#include "button-sensor.h"
#include "dev/leds.h"

#include "net/ip/uip-debug.h"

#include "802_15_4_test.h"

#define TIMEOUT		3 * CLOCK_SECOND
#define RADIO_ON    0
#define MAX_PAYLOAD_LEN		40

#define SENSOR_BUTTON_LEFT      &button_left_sensor
#define SENSOR_BUTTON_RIGHT     &button_right_sensor
#define CC26XX_DEMO_LEDS_BUTTON         LEDS_RED
#define CC26XX_DEMO_LEDS_REBOOT         LEDS_GREEN
#define NO_BTN_FOUND    255
#define NO_STATE_FOUND  255

/*---------------------------------------------------------------------------*/
PROCESS(test_802154_process, "test 802.15.4 process");
AUTOSTART_PROCESSES(&test_802154_process);
/*---------------------------------------------------------------------------*/
void
rxCallBack(char * packetStart, uint8_t len){
    //ToDo: print the received payload
}
/*---------------------------------------------------------------------------*/
uint16_t
getShortAddr(void){
    uint16_t short_addr;
    uint8_t ext_addr[8];
    ieee_addr_cpy_to(ext_addr, 8);
    short_addr = ext_addr[7];
    short_addr |= ext_addr[6] << 8;
    return short_addr;
}
/*---------------------------------------------------------------------------*/
static char buf[MAX_PAYLOAD_LEN];
static void
timeout_handler(void)
{
  int i,len;

  //ToDo: Prepare the 802.15.4 header

  //buf[0]  = (char[]){0x30,0x30, /*frame control*/ 0x80, 0x01, /*sequence*/0x00, IEEE802154_PANID};
  //getShortAddr(), 'f', 'o', 'o', 'b', 'a','r'};

//  buf[0] = 0x80;
//  buf[1] = 0x01;
//  buf[2] = 0x00;
//  buf[3] = (char)IEEE802154_PANID;
//  buf[4] = (IEEE802154_PANID >> 8);
//  buf[5] = (char)getShortAddr();
//  buf[6] = (getShortAddr()>>8);
//  buf[7] = 'f';
//  buf[8] = 'o';
//
//
//  len = 9;

  static uint8_t sequence=1;

  buf[0] = 0b01100001;
  buf[1]= 0b10001000;
  buf[2]= sequence;
  buf[3] = 0x02;
  buf[4] = 0x03;
  buf[5] = 0x05;
  buf[6] = 0x06;
  buf[7] = 0xAA;
  buf[8] = 0xBB;
  buf[9] = 0x30;
  buf[10] = 0x30;
  buf[11] = 'b';

  //len = 3;

  //ToDo: Fill some payload


  //Print the packet in Hex
  printf("\nBuffer in Hex:\n");
  len=strlen(buf);
  for(i=0;i<len;i++){
      if(i!=0 && i%4==0)printf(" ");
      if(i!=0 && i%8==0)printf("\n");
      printf("0x%02x ",buf[i]);
  }
  //print the packet as string
  printf("\n\nBuffer as string:\n");
  printf("%s\n\n", buf);

  //ToDo: send the packet

  NETSTACK_RADIO.send(buf,strlen(buf));

  //Check if the radio needs to stay on.
  if(!RADIO_ON){
      NETSTACK_RADIO.off();//Include this line to turn the radio off after rf operation
  }
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(test_802154_process, ev, data)
{
  static struct etimer et;


  PROCESS_BEGIN();
  PRINTF("802.15.4 process started\n");


  etimer_set(&et, TIMEOUT);
  if(RADIO_ON){
      NETSTACK_RADIO.on();//turn radio on to listen for packets
  }else{
      NETSTACK_RADIO.off();//turn radio off to save energy
  }
  while(1) {
    PROCESS_YIELD();
    if(etimer_expired(&et)) {
      timeout_handler();
      etimer_restart(&et);
    }else{
        printf("Event not handled: %d",ev);
    }
  }
  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
