/*
 * Copyright (c) 2014, Texas Instruments Incorporated - http://www.ti.com/
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/*---------------------------------------------------------------------------*/
/*	Prepared for educational use by /brts									 */
/*---------------------------------------------------------------------------*/
#include "contiki.h"
#include "sys/etimer.h"
#include "dev/leds.h"
#include "dev/watchdog.h"
#include "board-peripherals.h"
#include "rf-core/rf-ble.h"
#include "button-sensor.h"

#include "sensor_demo.h"
#include "sensor_demo_app.h"

#include <stdio.h>
#include <stdint.h>
/*---------------------------------------------------------------------------*/
#define CC26XX_DEMO_LOOP_INTERVAL       (CLOCK_SECOND * 5)
#define CC26XX_DEMO_LEDS_PERIODIC       LEDS_YELLOW
#define CC26XX_DEMO_LEDS_BUTTON         LEDS_RED
#define CC26XX_DEMO_LEDS_REBOOT         LEDS_ALL
/*---------------------------------------------------------------------------*/
#define SENSOR_BUTTON_LEFT      &button_left_sensor
#define SENSOR_BUTTON_RIGHT     &button_right_sensor
#define SENSOR_REED_RELAY    	&reed_relay_sensor
/*---------------------------------------------------------------------------*/
static struct etimer et;
/*---------------------------------------------------------------------------*/
PROCESS(sensor_demo_process, "Sensor Demo Process");
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(sensor_demo_process, ev, data)
{
	PROCESS_BEGIN();

	printf("CC26XX demo\n");
	  printf(sensor_demo_process.name);printf(": starting sensors_process\n");
	  process_start(&sensors_process, &sensor_demo_process);

	//initialize AON sensors
	init_sensors_aon();

	/* Init the BLE advertisement daemon */
	//rf_ble_beacond_config(0, BOARD_STRING);
	//rf_ble_beacond_start();

	//set timer for reading interval
	etimer_set(&et, CC26XX_DEMO_LOOP_INTERVAL);

	printf("-----------------------------------------\n");
	//initialize sensors
	printf(sensor_demo_process.name);printf(": activated all sensors.\n");
	activate_all_sensors();

	//first reading of the battery monitor
	get_batmon_reading();

	while(1)
	{
		//wait until events
		PROCESS_YIELD();
		//reading timer expired
		if(ev == PROCESS_EVENT_TIMER)
		{
			if(data == &et)
			{
				leds_toggle(CC26XX_DEMO_LEDS_PERIODIC);
				etimer_set(&et, CC26XX_DEMO_LOOP_INTERVAL);

				printf("-----------------------------------------\n");

				//first reading of the battery monitor again
				get_batmon_reading();

				//initialize sensors
				printf(sensor_demo_process.name);printf(": activated all sensors.\n");
				activate_all_sensors();
			}
		}
		//sensor event occurred
		else if(ev == sensors_event)
		{
			//left button pressed - will toggle CC26XX_DEMO_LEDS_BUTTON
			if(data == SENSOR_BUTTON_LEFT)
			{
				printf("Left: Pin %d, press duration %d clock ticks\n",
						(SENSOR_BUTTON_LEFT)->value(BUTTON_SENSOR_VALUE_STATE),
						(SENSOR_BUTTON_LEFT)->value(BUTTON_SENSOR_VALUE_DURATION));

				if((SENSOR_BUTTON_LEFT)->value(BUTTON_SENSOR_VALUE_DURATION) > CLOCK_SECOND)
				{
					printf("Long button press!\n");
				}

				leds_toggle(CC26XX_DEMO_LEDS_BUTTON);
			}
			//right button pressed - turns on LEDS_REBOOT and causes a watchdog reboot
			else if(data == SENSOR_BUTTON_RIGHT)
			{
				leds_on(CC26XX_DEMO_LEDS_REBOOT);
				watchdog_reboot();
			}
			//reed relay detected magnetic field
			else if(data == SENSOR_REED_RELAY)
			{
				if(buzzer_state())
				{
					buzzer_stop();
				}
				else
				{
					buzzer_start(1000);
				}
			}
			//as soon as the sensor is activated this event is called
			else if(data == &bmp_280_sensor)
			{
				//get and print the sensor reading
				get_bmp_reading();
			}
			//....
			else if(data == &opt_3001_sensor)
			{
				get_light_reading();
			}
			else if(data == &hdc_1000_sensor)
			{
				get_hdc_reading();
			}
			else if(data == &tmp_007_sensor)
			{
				get_tmp_reading();
			}
			else if(data == &mpu_9250_sensor)
			{
				get_mpu_reading();
			}
		}
	}

	PROCESS_END();
}
