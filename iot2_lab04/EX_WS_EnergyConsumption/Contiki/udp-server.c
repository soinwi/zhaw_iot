/*
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

#include "contiki.h"
#include "contiki-lib.h"
#include "contiki-net.h"

#include "sensors.h"
#include "button-sensor.h"
#include "dev/leds.h"

#include <string.h>
#include <stdlib.h>

#define DEBUG DEBUG_PRINT
#include "net/ip/uip-debug.h"

#define UIP_IP_BUF   ((struct uip_ip_hdr *)&uip_buf[UIP_LLH_LEN])

#define MAX_PAYLOAD_LEN 120

#define SENSOR_BUTTON_LEFT      &button_left_sensor
#define SENSOR_BUTTON_RIGHT     &button_right_sensor
#define CC26XX_DEMO_LEDS_BUTTON         LEDS_RED
#define CC26XX_DEMO_LEDS_REBOOT         LEDS_GREEN

#define MAX_CLIENTS     5

static struct uip_udp_conn *server_conn;

PROCESS(udp_server_process, "UDP server process");
AUTOSTART_PROCESSES(&udp_server_process);
/*---------------------------------------------------------------------------*/
#define NO_BTN_FOUND    255
#define NO_STATE_FOUND  255

static void
tcpip_handler(void)
{
  static int seq_id;
  char buf[MAX_PAYLOAD_LEN];
  char * markBTN = NULL;
  char * markStateRed = NULL;
  char * markStateGRN = NULL;
  uint8_t button, stateRed, stateGRN;
  uint8_t i, new;
  static uip_ipaddr_t clients[MAX_CLIENTS];
  static uint8_t      nrOfClients=0;

  if(uip_newdata()) {
    ((char *)uip_appdata)[uip_datalen()] = 0;
    PRINTF("Server received: '%s' from ", (char *)uip_appdata);
    PRINT6ADDR(&UIP_IP_BUF->srcipaddr);
    PRINTF("\n");

    new=true;
    for(i=0;i<nrOfClients;i++){
        if(uip_ipaddr_cmp(&UIP_IP_BUF->srcipaddr, &clients[i])){
            new=false;
            break;
        }
    }
    if(new){
        memcpy(&clients[nrOfClients],&UIP_IP_BUF->srcipaddr,sizeof(uip_ipaddr_t));
        if(nrOfClients<MAX_CLIENTS){
            nrOfClients++;
        }
    }

    if(strncmp((char *)uip_appdata,"Hello",5) == 0){
        uip_ipaddr_copy(&server_conn->ripaddr, &UIP_IP_BUF->srcipaddr);
        PRINTF("Responding with message: ");
        sprintf(buf, "Hello from the server! (%d)", ++seq_id);
        PRINTF("%s\n", buf);

        uip_udp_packet_send(server_conn, buf, strlen(buf));
        /* Restore server connection to allow data from any node */
        memset(&server_conn->ripaddr, 0, sizeof(server_conn->ripaddr));
    }else if(strncmp((char *)uip_appdata,"BTN: ",5) == 0){
        //get button and state start pointer
        markBTN = strstr((char *)uip_appdata,"BTN: ");
        PRINTF("Server decodes: '%s' \n", (char *)uip_appdata);
        markStateRed = strstr((char *)uip_appdata,"RED: ");
        markStateGRN = strstr((char *)uip_appdata,"GRN: ");
        if(markBTN && markStateRed && markStateGRN){
            //increase pointers to the value
            markBTN += 5;
            markStateRed += 5;
            markStateGRN += 5;
            button = strtol(markBTN,NULL,10);
            stateRed = strtol(markStateRed,NULL,10);
            stateGRN = strtol(markStateGRN,NULL,10);
        }else{
            button = NO_BTN_FOUND;
            stateRed = NO_STATE_FOUND;
            stateGRN = NO_STATE_FOUND;
        }
        PRINTF("Button value is %d, StateRed is: %d, StateGRN is: %d\n",button,stateRed,stateGRN);
        if(stateRed==1){
            leds_on(LEDS_RED);
        }else if(stateRed==0){
            leds_off(LEDS_RED);
        }
        if(stateGRN==1){
            leds_on(LEDS_GREEN);
        }else if(stateGRN==0){
            leds_off(LEDS_GREEN);
        }
        //Tell other clients about changes
        sprintf(buf, "%s", (char *)uip_appdata);
        for(i=0;i<nrOfClients;i++){
            uip_ipaddr_copy(&server_conn->ripaddr, &clients[i]);
            PRINTF("Send message to ");
            PRINT6ADDR(&clients[i]);
            PRINTF(":\n%s\n", buf);

            uip_udp_packet_send(server_conn, buf, strlen(buf));
        }
        /* Restore server connection to allow data from any node */
        memset(&server_conn->ripaddr, 0, sizeof(server_conn->ripaddr));
    }


  }
}
/*---------------------------------------------------------------------------*/
static void
print_local_addresses(void)
{
  int i;
  uint8_t state;

  PRINTF("Server IPv6 addresses: ");
  for(i = 0; i < UIP_DS6_ADDR_NB; i++) {
    state = uip_ds6_if.addr_list[i].state;
    if(uip_ds6_if.addr_list[i].isused &&
       (state == ADDR_TENTATIVE || state == ADDR_PREFERRED)) {
      PRINT6ADDR(&uip_ds6_if.addr_list[i].ipaddr);
      PRINTF("\n");
    }
  }
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(udp_server_process, ev, data)
{
#if UIP_CONF_ROUTER
  uip_ipaddr_t ipaddr;
#endif /* UIP_CONF_ROUTER */

  PROCESS_BEGIN();
  PRINTF("UDP server started\n");

  process_start(&resolv_process,NULL);

#if RESOLV_CONF_SUPPORTS_MDNS
  resolv_set_hostname("contiki-udp-server");
#endif

#if UIP_CONF_ROUTER
  uip_ip6addr(&ipaddr, 0xaaaa, 0, 0, 0, 0, 0, 0, 0);
  uip_ds6_set_addr_iid(&ipaddr, &uip_lladdr);
  uip_ds6_addr_add(&ipaddr, 0, ADDR_AUTOCONF);
#endif /* UIP_CONF_ROUTER */

  print_local_addresses();

  server_conn = udp_new(NULL, UIP_HTONS(3001), NULL);
  udp_bind(server_conn, UIP_HTONS(3000));

  while(1) {
    PROCESS_YIELD();
    if(ev == tcpip_event) {
      tcpip_handler();
    }
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
