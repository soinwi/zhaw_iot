/*
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

#include "contiki.h"
#include "contiki-lib.h"
#include "contiki-net.h"
#include "net/ip/resolv.h"

#include <string.h>
#include <stdbool.h>

#include "sensors.h"
#include "button-sensor.h"
#include "dev/leds.h"

#include "net/ip/uip-debug.h"

#include "802_15_4_test.h"

#define TIMEOUT		3 * CLOCK_SECOND
#define RADIO_ON    1
#define WAIT_ACK    0
#define MAX_PAYLOAD_LEN		125

#define SENSOR_BUTTON_LEFT      &button_left_sensor
#define SENSOR_BUTTON_RIGHT     &button_right_sensor
#define CC26XX_DEMO_LEDS_BUTTON         LEDS_RED
#define CC26XX_DEMO_LEDS_REBOOT         LEDS_GREEN
#define NO_BTN_FOUND    255
#define NO_STATE_FOUND  255

  static uint8_t sequence=1;

/*---------------------------------------------------------------------------*/
PROCESS(test_802154_process, "test 802.15.4 process");
AUTOSTART_PROCESSES(&test_802154_process);
/*---------------------------------------------------------------------------*/
void
rxCallBack(char * packetStart, uint8_t len){
    //Todo: Check if an ACK was received, check if it's the ACK for your packet.
    char * hdrStart = packetStart;

    if(*hdrStart == 0x02)
    {
    	if(len >= 3)
    	{
    		uint8_t seq = hdrStart[2];
    		if(seq == sequence)
    		{
    			PRINTF("\ncorrect ACK received!");
    		}
    	}
    }


    PRINTF("\nreceived:\n%s\n",hdrStart);
    PRINTF("HDR:\n%.9s\nPayload:\n%s\n",hdrStart,hdrStart+9);
}
/*---------------------------------------------------------------------------*/
uint16_t
getShortAddr(void){
    uint16_t short_addr;
    uint8_t ext_addr[8];
    ieee_addr_cpy_to(ext_addr, 8);
    short_addr = ext_addr[7];
    short_addr |= ext_addr[6] << 8;
    return short_addr;
}
/*---------------------------------------------------------------------------*/
static char buf[MAX_PAYLOAD_LEN];
static void
timeout_handler(void)
{
  int ret,i,len;

  sequence++;
  if(sequence==0){
      sequence=1;
  }

  //buf[0]=0b01000001;//7reserved(0)//6panIDcompression(1 intrapan)//5ackRequest(0)//4frame pending(0)//3security enabled(0)//2-0type(001=Data)
  buf[0]=0b01100001;//7reserved(0)//6panIDcompression(1 intrapan)//5ackRequest(1)//4frame pending(0)//3security enabled(0)//2-0type(001=Data)
  buf[1]=0b10001000;//15-14srcADDRmode(10 short)//13-12frameversion(00)//11-10destADDRmode(10 short)//9-8reserved(00)
  buf[2]=sequence;//sequence number
  buf[3]=IEEE802154_PANID & 0x00ff;//destpan lsb
  buf[4]=IEEE802154_PANID >> 8;//destpan msb

  //ToDo: change the destination address:
  buf[5]=0x21;//dest addr lsb
  buf[6]=0x42;//dest addr masb
  buf[7]=getShortAddr() & 0x00ff;//src addr lsb
  buf[8]=getShortAddr() >> 8;//src addr msb


  //ToDo: change the payload
  buf[9]=0x30;
  buf[10]=0x30;
  sprintf(buf+11,"0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789");

  //Print the packet in Hex
  printf("\nBuffer in Hex:\n");
  len=strlen(buf);
  for(i=0;i<len;i++){
      if(i!=0 && i%4==0)printf(" ");
      if(i!=0 && i%8==0)printf("\n");
      printf("0x%02x ",buf[i]);
  }

  //print the packet as string
  printf("\n\nBuffer as string:\n");
  printf("%s\n\n", buf);

  //Send the packet
  ret = NETSTACK_RADIO.send(buf, strlen(buf)); ///Contiki/cpu/cc26xx-cc13xx/rf-core/ieee-mode.c
  printf("Send returned: %d\n", ret);

  //ToDo: Turn the radio of or leave it on after sending a packet
  //NETSTACK_RADIO.off();
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(test_802154_process, ev, data)
{
  static struct etimer et;
  static struct etimer acktimer;


  PROCESS_BEGIN();
  PRINTF("802.15.4 process started\n");


  etimer_set(&et, TIMEOUT);
  etimer_set(&acktimer, )

  //ToDo: Turn the radio on or off before starting...
  while(1) {
    PROCESS_YIELD();
    if(etimer_expired(&et)) {
      timeout_handler();
      etimer_restart(&et);
    }else{
        printf("Event not handled: %d",ev);
    }
  }
  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
