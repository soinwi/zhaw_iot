package ch.zhaw.ines.leshanlwm2m.model;

import org.eclipse.leshan.client.resource.BaseInstanceEnabler;
import org.eclipse.leshan.core.node.LwM2mResource;
import org.eclipse.leshan.core.node.LwM2mSingleResource;
import org.eclipse.leshan.core.response.ReadResponse;
import org.eclipse.leshan.core.response.WriteResponse;

import grovepi.GrovePi;
import grovepi.Pin;
import grovepi.sensors.Led;

public class GroveLED extends BaseInstanceEnabler 
{
	private boolean ledState;
	private boolean outputPolarity;
	private String applicationType = null;
	
	private GrovePi grovePi = null;
	private Led jgroveLed = null;
	
	public GroveLED()
	{
		super();
		
		this.grovePi = new GrovePi();
		this.jgroveLed = grovePi.getDeviceFactory().createLed( Pin.DIGITAL_PIN_3 );
		
		this.ledState = jgroveLed.getState();
		this.outputPolarity = false;
		this.applicationType = "GroveLED";
	}
	
	
    public boolean getLedState() {
		return ledState;
	}


	public void setLedState(boolean ledState) {
		this.ledState = ledState;
	}


	public boolean getOutputPolarity() {
		return outputPolarity;
	}


	public void setOutputPolarity(boolean outputPolarity) {
		this.outputPolarity = outputPolarity;
	}


	public String getApplicationType() {
		return applicationType;
	}
	

	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}

	@Override
    public ReadResponse read( int resourceid ) 
    {
        System.out.println( "Read on LED Resource " + resourceid );
        switch( resourceid )
        {
        case 5550:
            return ReadResponse.success(LwM2mSingleResource.newBooleanResource(resourceid, getLedState()));
        case 5551:
            return ReadResponse.success(LwM2mSingleResource.newBooleanResource(resourceid, getOutputPolarity()));
        case 5750:
            return ReadResponse.success(LwM2mSingleResource.newStringResource(resourceid, getApplicationType()));
        default:
            return super.read( resourceid );
        }
    }
	
    @Override
    public WriteResponse write( int resourceid, LwM2mResource value ) 
    {
        WriteResponse retVal = null;
    	System.out.println( "Write on Device Resource " + resourceid + " value " + value );
        switch( resourceid ) 
        {
        case 5550:
            try
            {
	        	setLedState( ( boolean )value.getValue() );
	            jgroveLed.setState( ( boolean )value.getValue() );
	            fireResourcesChange( resourceid );
	            retVal = WriteResponse.success();
            }
            catch( ClassCastException e )
            {
            	retVal = WriteResponse.badRequest( e.getMessage() );
            }
            catch( Exception e )
            {
            	retVal = WriteResponse.internalServerError( e.getMessage() );
            }
            
        default:
        	retVal = super.write( resourceid, value );
        }
        
        return retVal;
    }
}
