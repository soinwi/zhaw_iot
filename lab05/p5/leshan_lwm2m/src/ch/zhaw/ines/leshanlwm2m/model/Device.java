package ch.zhaw.ines.leshanlwm2m.model;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import org.eclipse.leshan.client.resource.BaseInstanceEnabler;
import org.eclipse.leshan.core.node.LwM2mMultipleResource;
import org.eclipse.leshan.core.node.LwM2mResource;
import org.eclipse.leshan.core.node.LwM2mSingleResource;
import org.eclipse.leshan.core.response.ExecuteResponse;
import org.eclipse.leshan.core.response.ReadResponse;
import org.eclipse.leshan.core.response.WriteResponse;

public class Device extends BaseInstanceEnabler 
{
	public Device() 
	{
        // notify new date each 5 second
        Timer timer = new Timer();
        timer.schedule( new TimerTask() {
            @Override
            public void run() {
                fireResourcesChange(13);
            }
        }, 5000, 5000);
    }

    @Override
    public ReadResponse read(int resourceid) {
        System.out.println("Read on Device Resource " + resourceid);
        switch (resourceid) {
        case 0:
            return ReadResponse.success(LwM2mSingleResource.newStringResource(resourceid, getManufacturer()));
        case 1:
            return ReadResponse.success(LwM2mSingleResource.newStringResource(resourceid, getModelNumber()));
        case 2:
            return ReadResponse.success(LwM2mSingleResource.newStringResource(resourceid, getSerialNumber()));
        case 3:
            return ReadResponse.success(LwM2mSingleResource.newStringResource(resourceid, getFirmwareVersion()));
        case 9:
            return ReadResponse.success(LwM2mSingleResource.newIntegerResource(resourceid, getBatteryLevel()));
        case 10:
            return ReadResponse.success(LwM2mSingleResource.newIntegerResource(resourceid, getMemoryFree()));
        case 11:
            Map<Integer, Long> errorCodes = new HashMap<>();
            errorCodes.put(0, getErrorCode());
            return ReadResponse.success(LwM2mMultipleResource.newIntegerResource(resourceid, errorCodes));
        case 13:
            return ReadResponse.success(LwM2mSingleResource.newDateResource(resourceid, getCurrentTime()));
        case 14:
            return ReadResponse.success(LwM2mSingleResource.newStringResource(resourceid, getUtcOffset()));
        case 15:
            return ReadResponse.success(LwM2mSingleResource.newStringResource(resourceid, getTimezone()));
        case 16:
            return ReadResponse.success(LwM2mSingleResource.newStringResource(resourceid, getSupportedBinding()));
        default:
            return super.read(resourceid);
        }
    }

    @Override
    public ExecuteResponse execute(int resourceid, byte[] params) 
    {
        System.out.println( "Execute on Device resource " + resourceid );
        
        if ( params != null && params.length != 0 )
        {
        	System.out.println("\t params " + new String(params));
        }
        
        switch( resourceid )
        {
        	case 4:
        	{
        		/* handle the reboot of your device here ... */
        		System.out.println( "!!!Not yet implemented!!!" );
        		break;
        	}
        }
        
        return ExecuteResponse.success();
    }

    @Override
    public WriteResponse write(int resourceid, LwM2mResource value) {
        System.out.println("Write on Device Resource " + resourceid + " value " + value);
        switch (resourceid) {
        case 13:
            return WriteResponse.notFound();
        case 14:
            setUtcOffset((String) value.getValue());
            fireResourcesChange(resourceid);
            return WriteResponse.success();
        case 15:
            setTimezone((String) value.getValue());
            fireResourcesChange(resourceid);
            return WriteResponse.success();
        default:
            return super.write(resourceid, value);
        }
    }

    private String getManufacturer() {
        return "Raspberry Pi 2";
    }

    private String getModelNumber() {
        return "Model B";
    }

    private String getSerialNumber() {
        return "832-6274";
    }

    private String getFirmwareVersion() {
        return "Raspbian Jessie";
    }

    private long getErrorCode() {
        return 0;
    }

    private int getBatteryLevel() {
        final Random rand = new Random();
        return rand.nextInt(100);
    }

    private int getMemoryFree() {
        final Random rand = new Random();
        return rand.nextInt(50) + 114;
    }

    private Date getCurrentTime() {
        return new Date();
    }

    private String utcOffset = new SimpleDateFormat("X").format(Calendar.getInstance().getTime());;

    private String getUtcOffset() {
        return utcOffset;
    }

    private void setUtcOffset(String t) {
        utcOffset = t;
    }

    private String timeZone = TimeZone.getDefault().getID();

    private String getTimezone() {
        return timeZone;
    }

    private void setTimezone(String t) {
        timeZone = t;
    }

    private String getSupportedBinding() {
        return "U";
    }
}
