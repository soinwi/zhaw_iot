package ch.zhaw.ines.leshanlwm2m.util;

import org.eclipse.leshan.client.californium.LeshanClient;
import org.eclipse.leshan.core.request.DeregisterRequest;

public class Collector implements Runnable 
{
	private String registrationID;
	private LeshanClient client;
	
	public Collector( String registrationID, LeshanClient client ) 
	{
		super();
		this.registrationID = registrationID;
		this.client = client;
	}

	@Override
    public void run() 
	{
        if( registrationID != null ) 
        {
            System.out.println( "\tDevice: Deregistering Client '" + registrationID + "'" );
            client.send( new DeregisterRequest( registrationID ), 1000 );
            client.stop();
        }
    }
}
