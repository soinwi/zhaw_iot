/*******************************************************************************
 * Copyright (c) 2013-2015 Sierra Wireless and others.
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v1.0 which accompany this distribution.
 * 
 * The Eclipse Public License is available at
 *    http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at
 *    http://www.eclipse.org/org/documents/edl-v10.html.
 * 
 * Contributors:
 *     Zebra Technologies - initial API and implementation
 *     Sierra Wireless, - initial API and implementation
 *     Bosch Software Innovations GmbH, - initial API and implementation
 *******************************************************************************/

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.UUID;

import org.eclipse.leshan.ResponseCode;
import org.eclipse.leshan.client.californium.LeshanClient;
import org.eclipse.leshan.client.resource.LwM2mObjectEnabler;
import org.eclipse.leshan.client.resource.ObjectEnabler;
import org.eclipse.leshan.client.resource.ObjectsInitializer;
import org.eclipse.leshan.core.request.RegisterRequest;
import org.eclipse.leshan.core.response.RegisterResponse;

import ch.zhaw.ines.leshanlwm2m.model.Device;
import ch.zhaw.ines.leshanlwm2m.model.GroveLED;
import ch.zhaw.ines.leshanlwm2m.model.Location;
import ch.zhaw.ines.leshanlwm2m.util.Collector;

/*
 * To use:
 * java -jar target/leshan.jar <Server IP> 5683
 */
public class LeshanClientExample 
{
    private String registrationID;
    private final Location locationInstance = new Location();
    private final GroveLED groveLedInstance = new GroveLED();

    public static void main( final String[] args ) 
    {
        if( args.length != 4 && args.length != 2 ) 
        {
            System.out
                    .println( "Usage:\njava -jar target/leshan.jar [ClientIP] [ClientPort] ServerIP ServerPort" );
        } 
        else 
        {
            if( args.length == 4 )
            {
                new LeshanClientExample( args[0], Integer.parseInt( args[ 1 ] ), args[ 2 ], Integer.parseInt( args[ 3 ] ) );
            }
            else
            {
                new LeshanClientExample( "0", 0, args[ 0 ], Integer.parseInt( args[ 1 ] ) );
            }
        }
    }

    public LeshanClientExample( final String localHostName, final int localPort, 
    		                    final String serverHostName, final int serverPort )
    {   
    	// Initialize object list
        ObjectsInitializer initializer = new ObjectsInitializer();

        initializer.setClassForObject( 3, Device.class );
        initializer.setInstancesForObject( 6, locationInstance );
        initializer.setInstancesForObject( 3201, groveLedInstance );
        List<ObjectEnabler> enablers = initializer.createMandatory();
        enablers.add( initializer.create( 6 ) );
        enablers.add( initializer.create( 3201 ) );
        

        // Create client
        final InetSocketAddress clientAddress = new InetSocketAddress( localHostName, localPort );
        final InetSocketAddress serverAddress = new InetSocketAddress( serverHostName, serverPort );

        final LeshanClient client = new LeshanClient( clientAddress, serverAddress, 
        		                                      new ArrayList<LwM2mObjectEnabler>( enablers ) );

        // Start the client
        client.start();

        // Register to the server
        final String endpointIdentifier = UUID.randomUUID().toString();
        RegisterResponse response = client.send(new RegisterRequest(endpointIdentifier));
        if( response == null ) 
        {
            System.out.println( "Registration request timeout" );
            return;
        }

        System.out.println( "Device Registration (Success? " + response.getCode() + ")" );
        if( response.getCode() != ResponseCode.CREATED ) 
        {
            // TODO Should we have a error message on response ?
            // System.err.println("\tDevice Registration Error: " + response.getErrorMessage());
            System.err
                    .println( "If you're having issues connecting to the LWM2M endpoint, try using the DTLS port instead" );
            return;
        }

        registrationID = response.getRegistrationID();
        System.out.println( "\tDevice: Registered Client Location '" + registrationID + "'" );

        // Deregister on shutdown and stop client.
        Runtime.getRuntime().addShutdownHook( new Thread( new Collector( registrationID, client ) ) );
        
        // Change the location through the Console
        Scanner scanner = new Scanner(System.in);
        System.out.println("Press 'w','a','s','d' to change reported Location.");
        while (scanner.hasNext()) {
            String nextMove = scanner.next();
            locationInstance.moveLocation(nextMove);
        }
        scanner.close();
    }
}
