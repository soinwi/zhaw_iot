/*
 * Copyright (c) 2006, Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

/**
 * \file
 *         A very simple Contiki application showing how Contiki programs look
 * \author
 *         Adam Dunkels <adam@sics.se>
 */

#define NEW_MESSAGE		2
#define TEMPERATURE		1
#define MSG_SIZE		64
#define REPEAT_SEC      3

#include "contiki.h"

#include <stdio.h> /* For printf() */

#include "sensors.h"
#include "sensor_demo_app.h"


PROCESS_NAME(process1);
PROCESS_NAME(process2);
PROCESS_NAME(sensor_demo_process);


/*---------------------------------------------------------------------------*/
PROCESS(process1, "Process1");
AUTOSTART_PROCESSES(&process1);
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(process1, ev, data)
{
    //Timer for second clock
    static struct etimer etimer;
    //iteration counter
    static uint16_t counter = 0;
    //message buffer
    static char msg[MSG_SIZE];
    PROCESS_BEGIN();

    //start other processes
    printf(process1.name);printf(": running, starting process2\n");
    process_start(&process2,NULL);
    
    printf(process1.name);printf(": running, starting sensor_demo_process\n");
        process_start(&sensor_demo_process,NULL);

    etimer_set(&etimer, REPEAT_SEC*CLOCK_SECOND);
    while(1){
        //Wait for any event for this process
        PROCESS_WAIT_EVENT();
        printf(process1.name);printf(": ");
        //switch by event
        switch (ev) {
        case PROCESS_EVENT_TIMER:
            //increase counter and prepare message
            counter = counter + 1;
            sprintf(msg,"Hello there, World! Iteration: %i", counter);
            //make sure the message buffer is 0-terminated
            msg[MSG_SIZE-1]='\0';
            //Print the message
            printf("Send message to Pro2: \"");
            printf(msg);
            printf("\"");
            //Send the message to process2
            process_post(&process2, NEW_MESSAGE, msg);
            //restart the timer
            etimer_reset(&etimer);
            break;
        default:
            //unhandled events
            if(ev==sensors_event){//Do not handle sensor_events
                printf("Received sensor_event (ev: %d)", ev);
                break;
            }
            printf("Event not handled, event number %d", ev);
            break;
        }
        printf("\n");

    }

    PROCESS_END();
}
/*---------------------------------------------------------------------------*/



/*---------------------------------------------------------------------------*/
PROCESS(process2, "Process2");
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(process2, ev, data)
{
    PROCESS_BEGIN();
    //Print process name...
    printf(process2.name);printf(": running\n");
    while(1){
        //Wait for any event for this process
        PROCESS_WAIT_EVENT();
        printf(process2.name);printf(": ");
        //switch by event
        switch (ev) {
        case NEW_MESSAGE:
            //Print new messages
            printf("New message received: \"");
            printf(data);
            printf("\"");
            break;
        default:
            //unhandled events
            if(ev==sensors_event){//Do not handle sensor_events in this process
                printf("Received sensor_event (ev: %d)", ev);
                break;
            }
            printf("Event not handled, event number %d", ev);
            break;
        }
        printf("\n");
    }

    PROCESS_END();
}
/*---------------------------------------------------------------------------*/



